package jp.co.unirita.webapp.sample.service;

import jdk.nashorn.internal.runtime.regexp.joni.NodeOptInfo;
import jp.co.unirita.webapp.sample.constant.Constant;
import jp.co.unirita.webapp.sample.domain.expense.Expense;
import jp.co.unirita.webapp.sample.domain.expense.ExpenseRepository;
import jp.co.unirita.webapp.sample.domain.master.category.CategoryMaster;
import jp.co.unirita.webapp.sample.form.expense.ExpenseFormRow;
import jp.co.unirita.webapp.sample.form.expense.ExpensePanelRow;
import jp.co.unirita.webapp.sample.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ExpenseService {

    @Autowired
    CategoryService categoryService;
    @Autowired
    ExpenseRepository expenseRepository;

    public List<Expense> getExpenseSummaryOfCategory(long accountId, int year, int month) {
        Date from = DateUtil.createDate(year, month, 1);
        Date to = DateUtil.add(DateUtil.createDate(year, month + 1, 1), Calendar.DATE, -1);
        List<Expense> expenseList = expenseRepository.findByAccountIdAndDateBetween(accountId, from, to);
        Map<Long, List<Expense>> expenseMap = expenseList.stream()
                .collect(Collectors.groupingBy(Expense::getCategoryMasterId));
        return expenseMap.entrySet().stream()
                .map(e -> new Expense(accountId, e.getKey(), from, "",
                        e.getValue().stream().mapToInt(Expense::getPrice).sum()))
                .collect(Collectors.toList());
    }

    public List<ExpensePanelRow> getExpenseSummaryOfDate(long accountId, int year, int month) {
        Date from = DateUtil.createDate(year, month, 1);
        Date to = DateUtil.add(DateUtil.createDate(year, month + 1, 1), Calendar.DATE, -1);
        List<Expense> expenseList = expenseRepository.findByAccountIdAndDateBetween(accountId, from, to);
        Map<Date, List<Expense>> expenseMap = expenseList.stream()
                .collect(Collectors.groupingBy(Expense::getDate));
        return expenseMap.entrySet().stream()
                .map(e -> new ExpensePanelRow(e.getKey(), e.getValue().stream().mapToInt(Expense::getPrice).sum()))
                .sorted(Comparator.comparing(ExpensePanelRow::getDate).reversed())
                .collect(Collectors.toList());
    }

    public List<ExpensePanelRow> searchExpense(long accountId, Optional<Date> startDate, Optional<Date> endDate, long categoryId) {
        List<Expense> expenseList = expenseRepository
                .findByAccountIdAndDateBetween(accountId, startDate.orElse(DateUtil.MIN_DATE), endDate.orElse(DateUtil.MAX_DATE))
                .stream().filter(e -> categoryId == Constant.CATEGORY_NONE || e.getCategoryMasterId() == categoryId)
                .collect(Collectors.toList());
        Map<Long, String> categoryMap = categoryService.getAllCategory().stream()
                .collect(Collectors.toMap(CategoryMaster::getId, CategoryMaster::getName));
        return expenseList.stream()
                .map(e -> new ExpensePanelRow(e.getDate(), categoryMap.get(e.getCategoryMasterId()), e.getPrice()))
                .collect(Collectors.toList());
    }

    public void registerExpense(long accountId, Date date, List<ExpenseFormRow> rowList) {
        expenseRepository.saveAll(rowList.stream()
                .map(r -> new Expense(accountId, r.getCategoryId(), date, "", r.getPrice()))
                .collect(Collectors.toList()));
    }
}
