package jp.co.unirita.webapp.sample.form.expense;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ExpenseFormRow {
    private long categoryId;
    private String categoryName;
    private int price;
    private int content;
}
